#define I_KNOW_RARIAN_0_8_IS_UNSTABLE
#include "check-rarian.h"
#include "rarian-language.h"
#include "rarian-utils.h"
#include <check.h>
#include <stdlib.h>
#include <string.h>

START_TEST(test_rrn_language) {
    rrn_language_init("C");

    char **langs = rrn_language_get_langs();
    ck_assert_ptr_nonnull(langs);
    ck_assert_str_eq("C", langs[0]);

    free(langs);

    ck_assert_int_eq(0, rrn_language_use("Bogus", "MoreBogus"));
    ck_assert_int_eq(1, rrn_language_use("Bogus", "C"));
    ck_assert_int_eq(0, rrn_language_use("C", "C"));

    rrn_language_shutdown();
}
END_TEST

START_TEST(test_rrn_language_get_dirs) {
    rrn_language_init("C");
    char **dirs = rrn_language_get_dirs("/test/base");

    ck_assert_ptr_nonnull(dirs);
    ck_assert_str_eq("/test/base/LOCALE/C", dirs[0]);

    rrn_language_shutdown();

    rrn_freev(dirs);
}
END_TEST

Suite *rarian_language_suite() {
    Suite *s = suite_create("RarianLanguage");
    TCase *tc_rrn_reg = tcase_create("RrnLanguage");

    tcase_add_test(tc_rrn_reg, test_rrn_language);
    tcase_add_test(tc_rrn_reg, test_rrn_language_get_dirs);
    suite_add_tcase(s, tc_rrn_reg);

    return s;
}
