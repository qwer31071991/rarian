
#ifndef __CHECK_RARIAN_H
#define __CHECK_RARIAN_H

#include <check.h>

Suite *rarian_utils_suite();
Suite *rarian_reg_utils_suite();
Suite *rarian_list_utils_suite();
Suite *rarian_man_suite();
Suite *rarian_language_suite();
Suite *rarian_info_suite();
Suite *rarian_ht_utils_suite();
Suite *rarian_main_suite();

#endif
