

#include "rarian-ht-utils.h"
#include <check.h>
#include <stdlib.h>

typedef struct _Element Element;

struct _Element {
    Element *next;
    Element *prev;
};

static Element *element_new() { return calloc(1, sizeof(Element)); }

START_TEST(test_rrn_ht_basic) {
    Element *head = NULL;
    Element *tail = NULL;
    Element *first = element_new();
    Element *second = element_new();

    RRN_HT_PREPEND(head, tail, first);

    ck_assert_ptr_eq(head, first);
    ck_assert_ptr_eq(tail, first);

    RRN_HT_PREPEND(head, tail, second);

    ck_assert_ptr_eq(head, second);
    ck_assert_ptr_eq(tail, first);
    ck_assert_ptr_eq(head->next, first);
    ck_assert_ptr_eq(tail->prev, second);
    ck_assert_ptr_null(head->prev);
    ck_assert_ptr_null(tail->next);

    RRN_HT_REMOVE(head, tail, second);
    ck_assert_ptr_eq(head, first);
    ck_assert_ptr_eq(tail, first);
    ck_assert_ptr_null(head->prev);
    ck_assert_ptr_null(head->next);

    RRN_HT_REMOVE(head, tail, first);
    ck_assert_ptr_null(head);
    ck_assert_ptr_null(tail);

    RRN_HT_APPEND(head, tail, first);
    RRN_HT_APPEND(head, tail, second);

    ck_assert_ptr_eq(head, first);
    ck_assert_ptr_eq(tail, second);
    ck_assert_ptr_null(head->prev);
    ck_assert_ptr_null(tail->next);
    ck_assert_ptr_eq(first->next, second);
    ck_assert_ptr_eq(second->prev, first);

    free(first);
    free(second);
}
END_TEST

START_TEST(test_rrn_ht_replace) {
    int cnt = 0;
    Element *head = NULL;
    Element *tail = NULL;
    Element *first = element_new();
    Element *second = element_new();
    Element *third = element_new();
    Element *first_repl = element_new();
    Element *second_repl = element_new();
    Element *third_repl = element_new();

    RRN_HT_APPEND(head, tail, first);
    RRN_HT_APPEND(head, tail, second);
    RRN_HT_APPEND(head, tail, third);
    ck_assert_ptr_eq(head, first);
    ck_assert_ptr_eq(head->next, second);
    ck_assert_ptr_eq(tail, third);
    ck_assert_ptr_eq(tail->prev, second);

    /* Replace the middle element. */
    RRN_HT_REPLACE(head, tail, second, second_repl);
    ck_assert_ptr_null(second->prev);
    ck_assert_ptr_null(second->next);

    ck_assert_ptr_eq(first, second_repl->prev);
    ck_assert_ptr_eq(third, second_repl->next);

    /* Replace the element at the head of the list. */
    RRN_HT_REPLACE(head, tail, first, first_repl);
    ck_assert_ptr_eq(head, first_repl);
    ck_assert_ptr_eq(tail, third);
    ck_assert_ptr_null(first_repl->prev);
    ck_assert_ptr_eq(second_repl, first_repl->next);

    /* Replace the element at the tail of the list. */
    RRN_HT_REPLACE(head, tail, third, third_repl);
    ck_assert_ptr_eq(head, first_repl);
    ck_assert_ptr_eq(tail, third_repl);
    ck_assert_ptr_null(third_repl->next);
    ck_assert_ptr_eq(second_repl, third_repl->prev);

    RRN_HT_COUNT(head, tail, cnt);
    ck_assert_int_eq(cnt, 3);

    RRN_HT_REVERSE(head, tail);
    ck_assert_ptr_eq(head, third_repl);
    ck_assert_ptr_eq(tail, first_repl);
    ck_assert_ptr_eq(head->next, second_repl);
    ck_assert_ptr_eq(tail->prev, second_repl);

    RRN_HT_FREE(head, tail, free);
    free(first);
    free(second);
    free(third);
}
END_TEST

Suite *rarian_ht_utils_suite() {
    Suite *s = suite_create("RarianHtUtils");
    TCase *tc_rrn_ht = tcase_create("HT");

    tcase_add_test(tc_rrn_ht, test_rrn_ht_basic);
    tcase_add_test(tc_rrn_ht, test_rrn_ht_replace);
    suite_add_tcase(s, tc_rrn_ht);

    return s;
}
