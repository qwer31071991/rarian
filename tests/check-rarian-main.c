#define I_KNOW_RARIAN_0_8_IS_UNSTABLE
#include "rarian-main.h"
#include <check.h>

typedef struct CallBackCounter {
    size_t count;
} CallBackCounter;

static int counter_callback(void *reg, void *data) {
    CallBackCounter *counter = data;
    counter->count++;
    return FALSE;
}

static int first_reg(void *reg, void *data) {
    RrnReg **ret = data;
    RrnReg *this = reg;
    *ret = this;
    return TRUE;
}

START_TEST(test_rrn_main) {
    CallBackCounter counter;
    counter.count = 0;
    rrn_for_each(counter_callback, &counter);

    ck_assert_int_eq(counter.count, 1);

    rrn_shutdown();
}
END_TEST

START_TEST(test_rrn_main_lookup) {
    RrnReg *first = NULL;
    rrn_for_each(first_reg, &first);
    ck_assert_ptr_nonnull(first);

    ck_assert_ptr_eq(first, rrn_find_from_name(first->name));
    ck_assert_ptr_null(rrn_find_from_name("A definitely bogus name"));

    ck_assert_ptr_eq(first, rrn_find_from_ghelp(first->ghelp_name));
    ck_assert_ptr_null(rrn_find_from_ghelp("A definitely bogus name"));

    rrn_shutdown();
}
END_TEST

START_TEST(test_rrn_main_categories) {
    CallBackCounter counter;
    RrnReg *first = NULL;

    rrn_for_each(first_reg, &first);
    ck_assert_ptr_nonnull(first);
    ck_assert_ptr_nonnull(first->categories);
    ck_assert_ptr_nonnull(first->categories[0]);

    counter.count = 0;
    rrn_for_each_in_category(counter_callback, first->categories[0], &counter);

    ck_assert_int_eq(counter.count, 1);

    counter.count = 0;
    rrn_for_each_in_category(counter_callback, "A major bogus category",
                             &counter);

    ck_assert_int_eq(counter.count, 0);

    rrn_shutdown();
}
END_TEST

START_TEST(test_rrn_main_language) {
    /* Not much to actually test here, just execute the code paths to confirm
     * they don't blow up. */
    rrn_find_from_name("Force Init");
    rrn_set_language("C");
    rrn_shutdown();
}
END_TEST

Suite *rarian_main_suite() {
    Suite *s = suite_create("RarianMain");
    TCase *tc_rrn_main = tcase_create("RrnMain");

    tcase_add_test(tc_rrn_main, test_rrn_main);
    tcase_add_test(tc_rrn_main, test_rrn_main_lookup);
    tcase_add_test(tc_rrn_main, test_rrn_main_categories);
    tcase_add_test(tc_rrn_main, test_rrn_main_language);
    suite_add_tcase(s, tc_rrn_main);

    return s;
}
