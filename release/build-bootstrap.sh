#!/bin/bash

set -xe

dnf install -y \
    autoconf \
    automake \
    bzip2 \
    diffutils \
    gcc \
    gcc-c++ \
    libtool \
    make

