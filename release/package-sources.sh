#!/bin/bash

set -xe

VERSION=$1

if [ -z "$VERSION" ]; then
    echo "usage: package-sources.sh <VERSION>"
    exit 1
fi

OUT_DIR=packages
TOP_DIR=$(readlink -f $(dirname "${BASH_SOURCE[0]}")/../)
cd $TOP_DIR

NOCONFIGURE=1 ./autogen.sh

[ -d $OUT_DIR ] || mkdir $OUT_DIR

tar -cjf $OUT_DIR/rarian-$VERSION.tar.bz2 \
    --transform "s,^./,./rarian-$VERSION/," \
    --exclude autom4te.cache \
    --exclude $OUT_DIR \
    --exclude ".git" \
    .
