/*
 * rarian-list-utils.h
 * This file is part of Rarian
 *
 * Copyright (C) 2023 - Troy Curtis, Jr.
 *
 * Rarian is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rarian is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __RARIAN_LIST_UTILS_H
#define __RARIAN_LIST_UTILS_H
#ifdef __cplusplus
extern "C" {
#endif

#define RRN_LIST_PREPEND(head, elem)                                           \
    do {                                                                       \
        elem->prev = NULL;                                                     \
        elem->next = head;                                                     \
        if (head)                                                              \
            head->prev = elem;                                                 \
        head = elem;                                                           \
    } while (0)

#define RRN_LIST_REMOVE(head, del_elem)                                        \
    do {                                                                       \
        if (del_elem->prev)                                                    \
            del_elem->prev->next = del_elem->next;                             \
        if (del_elem->next)                                                    \
            del_elem->next->prev = del_elem->prev;                             \
        if (head == del_elem)                                                  \
            head = del_elem->next;                                             \
        del_elem->prev = NULL;                                                 \
        del_elem->next = NULL;                                                 \
    } while (0)

#define RRN_LIST_REPLACE(head, del_elem, ins_elem)                             \
    do {                                                                       \
        ins_elem->prev = del_elem->prev;                                       \
        ins_elem->next = del_elem->next;                                       \
        if (del_elem->prev)                                                    \
            del_elem->prev->next = ins_elem;                                   \
        if (del_elem->next)                                                    \
            del_elem->next->prev = ins_elem;                                   \
        if (head == del_elem)                                                  \
            head = ins_elem;                                                   \
        del_elem->prev = NULL;                                                 \
        del_elem->next = NULL;                                                 \
    } while (0)

#define RRN_LIST_FREE(head, free_fn)                                           \
    do {                                                                       \
        typeof(head) iter = head;                                              \
        while (iter) {                                                         \
            typeof(head) next = iter->next;                                    \
            free_fn(iter);                                                     \
            iter = next;                                                       \
        }                                                                      \
        head = NULL;                                                           \
    } while (0)

#define RRN_LIST_REVERSE(head)                                                 \
    do {                                                                       \
        for (typeof(head) iter = head; iter; iter = iter->prev) {              \
            typeof(head) new_prev = iter->next;                                \
            iter->next = iter->prev;                                           \
            iter->prev = new_prev;                                             \
            if (iter->prev == NULL) {                                          \
                head = iter;                                                   \
            }                                                                  \
        }                                                                      \
    } while (0)

#define RRN_LIST_COUNT(head, cnt)                                              \
    do {                                                                       \
        cnt = 0;                                                               \
        for (typeof(head) iter = head; iter; iter = iter->next) {              \
            cnt++;                                                             \
        }                                                                      \
    } while (0)

#ifdef __cplusplus
}
#endif
#endif
