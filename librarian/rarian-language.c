/*
 * rarian-language.c
 * This file is part of Rarian
 *
 * Copyright (C) 2006 - Don Scorgie
 *
 * Rarian is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rarian is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "rarian-language.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "rarian-list-utils.h"
#include "rarian-utils.h"

typedef struct _Lang Lang;

#ifndef FALSE
#define FALSE 0
#define TRUE !FALSE
#endif

struct _Lang {
    char *base;
    Lang *next;
    Lang *prev;
};

static Lang *lang_list;
static int nlangs = 0;

static void free_lang(Lang *in) {
    if (in) {
        free(in->base);
        free(in);
    }
}

static int check_lang(const char *lang) {
    for (Lang *iter = lang_list; iter; iter = iter->next) {
        if (!strcmp(iter->base, lang))
            return TRUE;
    }
    return FALSE;
}

static void add_lang(char *language) {
    Lang *lang = NULL;

    lang = malloc(sizeof(Lang));
    lang->base = language;
    lang->prev = NULL;
    RRN_LIST_PREPEND(lang_list, lang);
}

static int add_lang_dedup(char *lang) {
    if (check_lang(lang)) {
        free(lang);
        return FALSE;
    }

    add_lang(lang);
    return TRUE;
}

void rrn_language_init(char *lang) {
    char *loc = NULL;
    char *current = NULL;

    if (lang == NULL) {
        loc = getenv("LANGUAGE");
        if (!loc || !strcmp(loc, "")) {
            loc = getenv("LC_ALL");
        }
        if (!loc || !strcmp(loc, "")) {
            loc = getenv("LANG");
        }
        /* Make sure loc is allocated memory. */
        if (loc)
            loc = strdup(loc);
    } else {
        loc = strdup(lang);
    }
    nlangs = 0;

    if (!loc || !strcmp(loc, "")) {
        free(loc);
        loc = strdup("C");
    }

    current = loc;
    do {
        Lang *lang;
        char *at_mod;
        char *dot_mod;
        char *ter_mod;
        char *full_lang = NULL;
        char *exploded = NULL;
        int full_added = FALSE;

        char *tmp = strchr(current, ':');

        if (tmp)
            full_lang = rrn_strndup(current, tmp - current);
        else
            full_lang = strdup(current);

        at_mod = strrchr(full_lang, '@');
        dot_mod = strrchr(full_lang, '.');
        ter_mod = strrchr(full_lang, '_');

        /* Full lang first */
        if (!check_lang(full_lang)) {
            add_lang(full_lang);
            full_added = TRUE;
        }

        /* Lang sans modifier */
        if (at_mod) {
            exploded = rrn_strndup(full_lang, at_mod - full_lang);
            add_lang_dedup(exploded);
        }

        /* Lang sans modifier and codeset */
        if (dot_mod) {
            exploded = rrn_strndup(full_lang, dot_mod - full_lang);
            add_lang_dedup(exploded);
        }

        /* Lang sans modifier, codeset and territory */
        if (ter_mod) {
            exploded = rrn_strndup(full_lang, ter_mod - full_lang);
            add_lang_dedup(exploded);
        }

        if (!full_added)
            free(full_lang);

        if (tmp)
            current = tmp + 1;
        else
            current = NULL;
    } while (current);

    free(loc);

    add_lang_dedup(strdup("C"));

    RRN_LIST_REVERSE(lang_list);
    RRN_LIST_COUNT(lang_list, nlangs);
}

int rrn_language_use(char *current_language, char *proposed) {
    if (!lang_list)
        rrn_language_init(NULL);

    for (Lang *iter = lang_list; iter; iter = iter->next) {
        if (current_language && !strcmp(current_language, iter->base)) {
            return 0;
        }
        if (!strcmp(proposed, iter->base)) {
            return 1;
        }
    }
    return 0;
}

char **rrn_language_get_langs(void) {
    char **ret = NULL;
    int i = 0;

    if (!lang_list)
        rrn_language_init(NULL);

    ret = malloc(sizeof(char *) * (nlangs + 1));

    for (Lang *iter = lang_list; iter; iter = iter->next) {
        ret[i++] = iter->base;
    }
    ret[i] = NULL;
    return ret;
}

char **rrn_language_get_dirs(char *base) {
    char **ret = NULL;
    int i = 0;

    if (!lang_list)
        rrn_language_init(NULL);

    ret = malloc(sizeof(char *) * (nlangs + 1));

    for (Lang *iter = lang_list; iter; iter = iter->next) {
        ret[i++] = rrn_strconcat(base, "/LOCALE/", iter->base, NULL);
    }
    ret[i] = NULL;
    return ret;
}

void rrn_language_shutdown(void) { RRN_LIST_FREE(lang_list, free_lang); }
